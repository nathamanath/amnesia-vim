# Amnesia vim

> [Amnesia.io](https://amnesia.io) integration for vim.

This vim text editor package allows you to share code via amnesia.io directly
from vim, and prints out a share link for you to pass on.

Amnesia.io is an ephemeral code sharing service with nice collaboration and
privacy features. Read more about it at https://amnesia.io.

Also available for:
* [Sublime text 3](https://packagecontrol.io/packages/AmnesiaIO)
* [Atom](https://atom.io/packages/amnesia-io)

## Installation

Use your vim plugin manager of choice.

[Pathogen](https://github.com/tpope/vim-pathogen) installation example:

```sh
  git clone https://gitlab.com/nathamanath/amnesia-vim.git ~/.vim/bundle/amnesia-vim
```

**Depends on curl**

## Usage

Amnesia for vim exposes the following commands:

* `AmnShareLine` - Share the line under your cursor
* `AmnShareLines` - Share a range of lines

I map these like so in my `.vimrc`:

```vim
  " amnesia
  nnoremap <leader>al :AmnShareLine<CR>
  nnoremap <leader>af :AmnShareLines<CR>
```

You can also select lines in visual mode and share selection like so:

`:'<,'>AmnShareLines`
