if exists('g:loaded_amnesia')
  finish
endif
let g:loaded_amnesia = 1

let g:amnesia#ttl = 43200

" Share range of lines... default is whole file
command! -nargs=* -range=% AmnShareLines call amnesia#share_lines(<line1>,<line2>)
" Share current line
command! -nargs=* -range AmnShareLine call amnesia#share_lines(<line1>,<line2>)

function! amnesia#share_lines(from, to)
  :let content = join(getline(a:from, a:to), "\n")
  :call amnesia#post(content)
endfunction

function! amnesia#post(content)
  let data = {
    \ 'extension': expand('%:e'),
    \ 'content': a:content,
    \ 'ttl': g:amnesia#ttl
    \}

  :let out = system("curl -s -XPOST https://amnesia.io/api -H 'content-type: application/json' -d @-", json_encode(data))

  :let result = json_decode(out)
  :echom result['url']
endfunction
